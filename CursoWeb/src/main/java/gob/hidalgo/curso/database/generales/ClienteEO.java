package gob.hidalgo.curso.database.generales;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.PastOrPresent;
import javax.validation.constraints.Pattern;

import gob.hidalgo.curso.utils.EntityObject;
import org.apache.ibatis.type.Alias;

import java.util.List;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@Alias("ClienteEO")

public class ClienteEO extends EntityObject implements Serializable {

	private static final long serialVersionUID = 1L;
	private Integer id;
	@Pattern(regexp="^$|^[A-Z]{1}[AEIOU]{1}[A-Z]{2}[0-9]{2}(0[1-9]|1[0-2])(0[1-9]|1[0-9]|2[0-9]|3[0-1])[HM]{1}(AS|BC|BS|CC|CS|CH|CL|CM|DF|DG|GT|GR|HG|JC|MC|MN|MS|NT|NL|OC|PL|QT|QR|SP|SL|SR|TC|TS|TL|VZ|YN|ZS|NE)[B-DF-HJ-NP-TV-Z]{3}[0-9A-Z]{1}[0-9]{1}$",message="La clave CURP no es v�lida")
	private String curp;
	@NotBlank(message="Favor de anotar el nombre")
	private String nombre;
	@NotBlank(message="Favor de anotar el primer apellido")
	private String primerApellido;
	@NotBlank(message="Favor de anotar el segundo apellido")
	private String segundoApellido;
	@NotNull(message="Favor de seleccionar la fecha de nacimiento")
	@PastOrPresent(message="Favor de seleccionar una fecha de nacimiento v�lida")
	private LocalDate fechaNacimiento;
	@Email(message="El correo no es v�lido")
	private String mail;
	@NotBlank(message="Favor de anotar el celular")
	private String celular;
	
	private List<OrdenPagoEO> ordenesPago;
	
	public String getFechaNacimientoT() {
		String fecha;
		if (fechaNacimiento != null) {
			fecha = fechaNacimiento.format(DateTimeFormatter.ofPattern("dd/MM/yyyy"));
		}else {
			fecha="";
		}
		return fecha;
	}
}
