package gob.hidalgo.curso.components.generales;

import java.util.HashMap;
import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import gob.hidalgo.curso.utils.Modelo;
import gob.hidalgo.curso.components.MensajesC;
import gob.hidalgo.curso.database.generales.ClienteEO;
import gob.hidalgo.curso.database.generales.OrdenPagoEO;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component("OrdenesPagoC")
public class OrdenesPagoC {

	@Autowired
	private SqlSession sqlSession;
	
	@Autowired
	private MensajesC mensajesC;
	
	public OrdenesPagoC() {
		super();
		log.debug("Se crea el componenete Ordenes de Pago");
	}
	
	public Modelo<OrdenPagoEO> modelo(ClienteEO cliente){
		List<OrdenPagoEO> listado;
		listado = sqlSession.selectList("generales.ordenesPago.porCliente",cliente);
		return new Modelo<OrdenPagoEO>(listado);
	}
	
	public OrdenPagoEO nuevo() {
		return new OrdenPagoEO();
	}
	
	public boolean guardar(ClienteEO cliente, OrdenPagoEO orden) {
		HashMap<String,Object> mapaParametros;
		
		if (orden.getId()  == null) {
			mapaParametros = new HashMap<String, Object>();
			mapaParametros.put("Cliente", cliente);
			mapaParametros.put("ordenPago", orden);
			sqlSession.insert("generales.ordenesPago.insertar", mapaParametros);
			mensajesC.mensajeInfo("Orden de pago se insert� correctamemte");
		}else {
			sqlSession.update("generales.ordenesPago.actualizar", orden);
			mensajesC.mensajeInfo("Orden de Pago actualizada correctamente");
		}
		return true;
	}
}
